{{ config(materialized='table') }}
with sf_account as (

    select
        id as account_id,
        name as account_name

    from postgres.demo.account

),

csv_account as (

    select
        id as pid,
        account_id as account_id,
        amount as csv_amount

    from postgres.demo.account_csv

),

total_amount as (

    select
        account_id,

        sum(csv_amount) as total_amt

    from csv_account

    group by 1

),


final as (

    select
        sf_account.account_id,
        sf_account.account_name,
        total_amount.total_amt

    from sf_account

    left join total_amount using (account_id)

)

select * from final